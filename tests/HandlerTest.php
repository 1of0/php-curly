<?php

namespace OneOfZero\Curly\Tests;

use OneOfZero\Curly\Curly;
use OneOfZero\Curly\Handlers\AbstractHandler;
use OneOfZero\Curly\Handlers\HandlerInterface;
use OneOfZero\Curly\SharedStream;
use OneOfZero\Curly\Tests\Fixtures\LoggingHandler;

class HandlerTest extends AbstractTestCase
{
    private const BYTES_TO_READ = 102400;

    /**
     * Checks whether all events are properly triggered in a handler.
     */
    public function testEventOrder(): void
    {
        $curly = new Curly();

        $handler = new LoggingHandler();
        $curly->setCustomHandler($handler);

        $inputStream = $this->createRandomByteStream();

        $request = $this->buildRequest('POST', 'post')
            ->withBody($inputStream)
            ->withAddedHeader('Content-Type', 'application/octet-stream')
        ;
        $response = $curly->request($request);
        $log = $handler->getLogStack();

        $this->assertNotEmpty($log, 'The handler did not process any data');

        $events = array_map(static function($entry) { return $entry['event']; }, $log);

        $this->assertContains(HandlerInterface::ON_PROGRESS, $events, 'The handler did not handle any progress events');
        $this->assertContains(HandlerInterface::ON_HEADER, $events, 'The handler did not handle any header events');
        $this->assertContains(HandlerInterface::ON_READ, $events, 'The handler did not handle any read events');
        $this->assertContains(HandlerInterface::ON_WRITE, $events, 'The handler did not handle any write events');

        $headersCounted = 0;
        $writesCounted = 0;
        $readsCounted = 0;

        foreach ($events as $event) {
            switch ($event) {
                case HandlerInterface::ON_HEADER:
                    $headersCounted++;

                    if ($writesCounted) {
                        // Header events must not occur after writing has started.
                        $this->fail('Handled header event after writing');
                    }
                    break;

                case HandlerInterface::ON_WRITE:
                    $writesCounted++;
                    break;

                case HandlerInterface::ON_READ:
                    $readsCounted++;

                    if ($headersCounted !== 2) {
                        // Before reading 2 header events are expected. The first one carrying the 100 status code, and
                        // the second one carrying a line break.
                        //
                        // The third header and those following will pertain to the response. And while the response is
                        // being emitted by the server, no read events should be taking place.
                        $this->fail("Handled $headersCounted header events before reading. This should always be 2.");
                    }

                    if ($writesCounted) {
                        // Read events must not occur after writing has started.
                        $this->fail('Handled read event after writing');
                    }
                    break;
            }
        }

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Creates a stream in memory of BYTES_TO_READ random bytes.
     *
     * @return SharedStream
     */
    private function createRandomByteStream(): SharedStream
    {
        $stream = new SharedStream('php://temp', 'r+b');

        for ($i = 0; $i < self::BYTES_TO_READ; $i++) {
            $stream->write(chr(($i % 26) + 97));
        }

        $stream->rewind();
        return $stream;
    }
}
