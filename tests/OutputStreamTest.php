<?php

namespace OneOfZero\Curly\Tests;

use OneOfZero\Curly\BinarySafe;
use OneOfZero\Curly\Curly;
use OneOfZero\Curly\Handlers\StreamHandler;
use OneOfZero\Curly\SharedStream;

class OutputStreamTest extends AbstractTestCase
{
    private const BYTES_TO_READ = 102400;

    /**
     * Checks whether output streams work properly in a PSR-7 response.
     */
    public function testDefaultStreamSetup(): void
    {
        $curly = new Curly();

        $request = $this->buildRequest('GET', 'bytes', self::BYTES_TO_READ);
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());
        $body = strval($response->getBody());
        $this->assertEquals(self::BYTES_TO_READ, BinarySafe::strlen($body));
    }

    /**
     * Checks whether output streams work properly when configured through a StreamHandler.
     */
    public function testStreamHandler(): void
    {
        $curly = new Curly();

        $stream = new SharedStream('php://temp', 'r+b');
        $curly->setCustomHandler(new StreamHandler(null, $stream));

        $request = $this->buildRequest('GET', 'bytes', self::BYTES_TO_READ);
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(self::BYTES_TO_READ, $stream->getSize());
    }

    /**
     * Checks whether output streams work properly when configured through the CurlyOptions.
     */
    public function testStreamOption(): void
    {
        $curly = new Curly();

        $stream = new SharedStream('php://temp', 'r+b');
        $curly->getOptions()->outputStream = $stream->getResource();

        $request = $this->buildRequest('GET', 'bytes', self::BYTES_TO_READ);
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(self::BYTES_TO_READ, $stream->getSize());
    }
}
