<?php

namespace OneOfZero\Curly\Tests;

use OneOfZero\Curly\ExtendedServerRequest;
use PHPUnit\Framework\TestCase;
use Laminas\Diactoros\Uri;

abstract class AbstractTestCase extends TestCase
{
    /**
     * @var string
     */
    protected $scheme = 'http';

    /**
     * @var string
     */
    protected $host = '127.0.0.1';

    /**
     * @var int
     */
    protected $port;

    public function setUp(): void
    {
        $this->port = Bootstrapper::get()->getPort();
    }

    protected function getBaseUri(): string
    {
        return sprintf('%s://%s:%s/', $this->scheme, $this->host, $this->port);
    }

    protected function buildUri(array $pieces): Uri
    {
        return new Uri($this->getBaseUri() . implode('/', $pieces));
    }

    protected function buildRequest(string $method, string ...$url): ExtendedServerRequest
    {
        return (new ExtendedServerRequest)
            ->withMethod($method)
            ->withUri($this->buildUri($url));
    }
}
