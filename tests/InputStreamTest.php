<?php

namespace OneOfZero\Curly\Tests;

use OneOfZero\Curly\Curly;
use OneOfZero\Curly\Handlers\StreamHandler;
use OneOfZero\Curly\SharedStream;

class InputStreamTest extends AbstractTestCase
{
    private const BYTES_TO_READ = 1024;

    /**
     * Checks whether output streams work properly when configured through a StreamHandler.
     */
    public function testStreamHandler(): void
    {
        $curly = new Curly();

        $inputStream = $this->createRandomByteStream();

        $curly->setCustomHandler(new StreamHandler($inputStream));

        $request = $this->buildRequest('POST', 'post')
            ->withAddedHeader('Content-Type', 'text/plain')
            ->withAddedHeader('Content-Length', strval(self::BYTES_TO_READ));

        $response = $curly->request($request);
        $this->assertEquals(200, $response->getStatusCode());

        $responseObject = json_decode($response->getBody()->getContents());
        $this->assertEquals(strval($inputStream), $responseObject->data);
    }

    /**
     * Creates a stream in memory of BYTES_TO_READ random bytes.
     *
     * @return SharedStream
     */
    private function createRandomByteStream(): SharedStream
    {
        $stream = new SharedStream('php://temp', 'r+b');

        for ($i = 0; $i < self::BYTES_TO_READ; $i++) {
            $stream->write(chr(($i % 26) + 97));
        }

        $stream->rewind();
        return $stream;
    }
}
