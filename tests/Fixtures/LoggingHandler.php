<?php

namespace OneOfZero\Curly\Tests\Fixtures;

use OneOfZero\Curly\BinarySafe;
use OneOfZero\Curly\Handlers\AbstractHandler;

class LoggingHandler extends AbstractHandler
{
    /**
     * @var array $logStack
     */
    private $logStack = [];

    /**
     * {@inheritdoc}
     */
    public function getImplemented(): array
    {
        return self::VALID_EVENTS;
    }

    /**
     * {@inheritdoc}
     */
    public function onHeader($channel, string $headerData): int
    {
        $this->logStack[] = [
            'event' => self::ON_HEADER,
            'data' => $headerData
        ];
        return BinarySafe::strlen($headerData);
    }

    /**
     * {@inheritdoc}
     */
    public function onProgress(
        $channel,
        int $downloadBytesTotal,
        int $downloadedBytes,
        int $uploadBytesTotal,
        int $uploadedBytes
    ): int {
        $this->logStack[] = [
            'event' => self::ON_PROGRESS,
            'data' => sprintf(
                'Download %s/%s - Upload %s/%s',
                $downloadedBytes,
                $downloadBytesTotal,
                $uploadedBytes,
                $uploadBytesTotal
            )
        ];
        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function onRead($channel, $stream, int $bufferSize): string
    {
        $this->logStack[] = [
            'event' => self::ON_READ,
            'data' => $bufferSize
        ];
        return fread($stream, $bufferSize);
    }

    /**
     * {@inheritdoc}
     */
    public function onWrite($channel, string $data): int
    {
        $this->logStack[] = [
            'event' => self::ON_WRITE,
            'data' => $data
        ];
        return BinarySafe::strlen($data);
    }

    public function getLogStack(): array
    {
        return $this->logStack;
    }
}
