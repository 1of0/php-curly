<?php

/** @noinspection PhpComposerExtensionStubsInspection */

namespace OneOfZero\Curly;

use Psr\Http\Message\StreamInterface;
use RuntimeException;

BinarySafe::initialize();

/**
 * Class BinarySafe
 *
 * Collection of helper functions that ensure proper function when dealing with unicode data.
 */
class BinarySafe
{
    /**
     * Default maximum for write failures
     */
    private const MAX_WRITE_FAILURES = 5;

    /**
     * @var bool
     */
    private static $mbFunctionsAvailable = false;

    /**
     * Detects whether mb_* functions are available.
     */
    public static function initialize(): void
    {
        self::$mbFunctionsAvailable = function_exists('mb_strlen');
    }

    /**
     * Binary-safe strlen implementation; uses mb_strlen if available.
     *
     * @param string $input
     * @return int
     * @throws RuntimeException
     */
    public static function strlen(string $input): int
    {
        if (self::$mbFunctionsAvailable) {
            $length = mb_strlen($input, '8bit');
            if ($length === false) {
                throw new RuntimeException('mb_strlen() failed');
            }
            return $length;
        }

        return strlen($input);
    }

    /**
     * Binary-safe substr implementation; uses mb_substr if available.
     *
     * @param string $input
     * @param int $start
     * @param int|null $length
     * @return string
     */
    public static function substr(string $input, int $start, ?int $length = null): string
    {
        if ($start === 0 && $length === null) {
            return $input;
        }

        if (self::$mbFunctionsAvailable) {
            if (!isset($length))  {
                $length = ($start >= 0)
                    ? self::strlen($input) - $start
                    : -$start;
            }
            return mb_substr($input, $start, $length, '8bit');
        }

        return isset($length)
            ? substr($input, $start, $length)
            : substr($input, $start);
    }

    /**
     * Robust stream writing method that has tolerance for write failures.
     *
     * @param StreamInterface $stream
     * @param string $data
     * @param int $maxWriteFailures
     * @return int
     */
    public static function write(
        StreamInterface $stream,
        string $data,
        int $maxWriteFailures = self::MAX_WRITE_FAILURES
    ): int {
        $bytesToWrite = self::strlen($data);
        $bytesWrittenTotal = 0;
        $failCounter = 0;

        while ($bytesWrittenTotal < $bytesToWrite) {
            $bytesWritten = $stream->write(self::substr($data, $bytesWrittenTotal));

            if ($bytesWritten === false) {
                return $bytesWrittenTotal;
            }

            $failCounter = ($bytesWritten === 0)
                ? $failCounter + 1
                : 0;

            if ($failCounter === $maxWriteFailures) {
                return $bytesWrittenTotal;
            }

            $bytesWrittenTotal += $bytesWritten;
        }

        return $bytesWrittenTotal;
    }
}
