<?php

namespace OneOfZero\Curly\Handlers;

use OneOfZero\Curly\BinarySafe;
use OneOfZero\Curly\CurlyOptions;

/**
 * Class AbstractHandler
 *
 * Defines an abstract handler for cURL callback functions.
 */
abstract class AbstractHandler implements HandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function registerCallbacks(CurlyOptions $options): void
    {
        foreach ($this->getImplemented() as $option) {
            if (!in_array($option, self::VALID_EVENTS)) {
                continue;
            }

            $options->{$option} = [ $this, $option ];

            if ($option === self::ON_PROGRESS) {
                $options->noProgress = false;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function onHeader($channel, string $headerData): int
    {
        return BinarySafe::strlen($headerData);
    }

    /**
     * {@inheritDoc}
     */
    public function onProgress(
        $channel,
        int $downloadBytesTotal,
        int $downloadedBytes,
        int $uploadBytesTotal,
        int $uploadedBytes
    ): int {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    public function onRead($channel, $stream, int $bufferSize): string
    {
        return fread($stream, $bufferSize);
    }

    /**
     * {@inheritDoc}
     */
    public function onWrite($channel, string $data): int
    {
        return BinarySafe::strlen($data);
    }
}
