<?php

namespace OneOfZero\Curly\Handlers;

use OneOfZero\Curly\CurlyOptions;

interface HandlerInterface
{
    public const ON_HEADER = 'onHeader';
    public const ON_PROGRESS = 'onProgress';
    public const ON_READ = 'onRead';
    public const ON_WRITE = 'onWrite';

    public const VALID_EVENTS = [
        self::ON_HEADER,
        self::ON_PROGRESS,
        self::ON_READ,
        self::ON_WRITE
    ];

    /**
     * Should return an array with the function names of the events that are implemented. The only supported events are:
     * 'onHeader', 'onProgress', 'onRead', and 'onWrite'.
     *
     * @return string[]
     */
    public function getImplemented(): array;

    /**
     * Registers the functions specified by getImplemented() as callback functions in the provided CurlyOptions
     * instance.
     *
     * This method is for internal use only and should never be used.
     *
     * @param CurlyOptions $options
     */
    public function registerCallbacks(CurlyOptions $options): void;

    /**
     * Fired when a header is received from the server.
     *
     * The provided header data yields a single line of the received header each time that it is called.
     *
     * The return value must be the size of the header data in bytes. To return this be sure to call the parent, or
     * manually return the size (it's advisable to use BinarySafe::strlen).
     *
     * @see http://curl.haxx.se/libcurl/c/CURLOPT_HEADERFUNCTION.html
     *
     * @param resource $channel
     * @param string $headerData
     *
     * @return int
     */
    public function onHeader($channel, string $headerData): int;

    /**
     * Fired frequently during transfer.
     *
     * Provides upload and download statistics from the request.
     *
     * Returning non-zero from this method will abort the transfer.
     *
     * @see http://curl.haxx.se/libcurl/c/CURLOPT_PROGRESSFUNCTION.html
     *
     * @param resource $channel
     * @param int $downloadBytesTotal
     * @param int $downloadedBytes
     * @param int $uploadBytesTotal
     * @param int $uploadedBytes
     * @return int
     */
    public function onProgress(
        $channel,
        int $downloadBytesTotal,
        int $downloadedBytes,
        int $uploadBytesTotal,
        int $uploadedBytes
    ): int;

    /**
     * Fired when the library is ready to transfer data to the server.
     *
     * Provides the configured stream, and the size of the buffer.
     *
     * The return value must yield the data to be transferred. Note that the size of the return value may not exceed
     * the provided buffer size. Returning 0 will abort the transfer, but note that sending less than the server is
     * expecting, may cause the connection to hang.
     *
     * @see http://curl.haxx.se/libcurl/c/CURLOPT_READFUNCTION.html
     *
     * @param resource $channel
     * @param resource $stream
     * @param int $bufferSize
     *
     * @return string
     */
    public function onRead($channel, $stream, int $bufferSize): string;

    /**
     * Fired when data is received from the server.
     *
     * Provides the received data.
     *
     * The return value must be the size of the received data in bytes. To return this be sure to call the parent, or
     * manually return the size (it's advisable to use BinarySafe::strlen)
     *
     * @see http://curl.haxx.se/libcurl/c/CURLOPT_WRITEFUNCTION.html
     *
     * @param resource $channel
     * @param string $data
     * @return int
     */
    public function onWrite($channel, string $data): int;
}
