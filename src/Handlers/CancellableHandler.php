<?php

namespace OneOfZero\Curly\Handlers;

use OneOfZero\Curly\CancellationCallbackInterface;

/**
 * Class CancellableHandler
 *
 * Handler implementation that allows cancellation of a transfer by callback.
 *
 * Note that this handler hooks into the progress event, which runs synchronously in the transfer. If the callback
 * method is not fast enough, transfer speed may suffer.
 */
class CancellableHandler extends AbstractHandler
{
    /**
     * Holds an instance of the cancellation callback object.
     *
     * @var CancellationCallbackInterface
     */
    protected $cancellationCallback;

    /**
     * Creates an instance of the CancellableHandler.
     *
     * If no cancellation callback is provided, the progress event will not be enabled.
     *
     * @param CancellationCallbackInterface|null $cancellationCallback
     */
    public function __construct(CancellationCallbackInterface $cancellationCallback = null)
    {
        $this->cancellationCallback = $cancellationCallback;
    }

    /**
     * {@inheritdoc}
     */
    public function getImplemented(): array
    {
        return $this->cancellationCallback !== null
            ? [ self::ON_PROGRESS ]
            : [];
    }

    /**
     * {@inheritdoc}
     */
    public function onProgress(
        $channel,
        int $downloadBytesTotal,
        int $downloadedBytes,
        int $uploadBytesTotal,
        int $uploadedBytes
    ): int {
        return $this->cancellationCallback->isCanceled() ? 1 : 0;
    }
}
