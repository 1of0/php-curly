<?php

namespace OneOfZero\Curly\Handlers;

use InvalidArgumentException;
use OneOfZero\Curly\BinarySafe;
use OneOfZero\Curly\CancellationCallbackInterface;
use OneOfZero\Curly\CurlyOptions;
use OneOfZero\Streams\SharedStreamInterface;
use RuntimeException;

/**
 * Class StreamHandler
 *
 * Handler implementation that allows manual configuration of the input and output streams.
 *
 * Provides the option to forward headers directly to the PHP output.
 */
class StreamHandler extends CancellableHandler
{
    /**
     * @var SharedStreamInterface
     */
    protected $inputStream;

    /**
     * @var SharedStreamInterface
     */
    protected $outputStream;

    /**
     * @var bool
     */
    protected $forwardsOutputHeader = false;

    /**
     * Creates an instance of the StreamHandler, optionally providing an input or output stream, or a cancellation
     * token.
     *
     * @param SharedStreamInterface|null $inputStream
     * @param SharedStreamInterface|null $outputStream
     * @param CancellationCallbackInterface|null $cancellationToken
     */
    public function __construct(
        ?SharedStreamInterface $inputStream = null,
        ?SharedStreamInterface $outputStream = null,
        ?CancellationCallbackInterface $cancellationToken = null
    ) {
        parent::__construct($cancellationToken);

        if ($inputStream !== null && !$inputStream->isReadable()) {
            throw new InvalidArgumentException('The provided input stream is not readable');
        }
        $this->inputStream = $inputStream;

        if ($outputStream !== null && !$outputStream->isWritable()) {
            throw new InvalidArgumentException('The provided output stream is not writable');
        }
        $this->outputStream = $outputStream;
    }

    /**
     * {@inheritdoc}
     */
    public function getImplemented(): array
    {
        $implemented = parent::getImplemented();

        if ($this->inputStream) {
            $implemented[] = self::ON_READ;
        }

        if ($this->outputStream !== null) {
            $implemented[] = self::ON_WRITE;
        }

        if ($this->forwardsOutputHeader) {
            $implemented[] = self::ON_HEADER;
        }

        return $implemented;
    }

    /**
     * {@inheritdoc}
     */
    public function registerCallbacks(CurlyOptions $options): void
    {
        parent::registerCallbacks($options);

        if ($this->inputStream) {
            $options->inputStream = $this->inputStream->getResource();
        }

        if ($this->outputStream) {
            $options->outputStream = $this->outputStream->getResource();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function onRead($channel, $stream, int $bufferSize): string
    {
        $buffer = '';
        $bytesToRead = $bufferSize;
        $bytesReadTotal = 0;
        while ($bytesReadTotal < $bufferSize && !$this->inputStream->eof()) {
            $buffer .= $this->inputStream->read($bytesToRead);
            $bytesReadTotal = BinarySafe::strlen($buffer);
            $bytesToRead = $bufferSize - $bytesReadTotal;
        }
        return $buffer;
    }

    /**
     * {@inheritdoc}
     */
    public function onWrite($channel, string $data): int
    {
        return BinarySafe::write($this->outputStream, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function onHeader($channel, string $headerData): int
    {
        if (!$this->forwardsOutputHeader) {
            return parent::onHeader($channel, $headerData);
        }

        // Don't forward 100 status responses
        if (stripos($headerData, '100 Continue') !== false) {
            return parent::onHeader($channel, $headerData);
        }

        // Skip empty header lines
        if ($headerData === "\r\n") {
            return 2;
        }

        // Trim off line break
        [$header] = explode("\r\n", $headerData);

        if (headers_sent()) {
            throw new RuntimeException('Cannot forward headers because headers were already sent');
        }

        header($header, false);

        return parent::onHeader($channel, $headerData);
    }

    /**
     * Returns whether or not the received headers are automatically forwarded to the PHP output.
     *
     * @return bool
     */
    public function forwardsOutputHeader(): bool
    {
        return $this->forwardsOutputHeader;
    }

    /**
     * Enable automatic header forwarding to the PHP output.
     */
    public function enableOutputHeaderForwarding(): void
    {
        $this->forwardsOutputHeader = true;
    }

    /**
     * Disables automatic header forwarding to the PHP output.
     */
    public function disableOutputHeaderForwarding(): void
    {
        $this->forwardsOutputHeader = false;
    }
}
