<?php

namespace OneOfZero\Curly;

use OneOfZero\Streams\SharedStreamInterface;
use Laminas\Diactoros\Stream;

/**
 * Class SharedStream
 *
 * Extends the Zend Stream object by providing an additional method that allows to retrieve the inner stream resource
 * without detaching it.
 */
class SharedStream extends Stream implements SharedStreamInterface
{
    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return $this->resource;
    }
}
