<?php

/** @noinspection PhpUnused */

namespace OneOfZero\Curly;

use ReflectionClass;

/**
 * Class CurlyOptions
 *
 * Makes supported cURL options available through properties.
 *
 * Note that some properties have different names from the options they represent. See the code file for a mapping
 * between the properties and the CURLOPT_* constants.
 */
class CurlyOptions
{
    #region // Mapping between property names and cURL constants

    protected const OPTIONS_MAP = [
        'followRedirects' => CURLOPT_FOLLOWLOCATION,
        'maxRedirects' => CURLOPT_MAXREDIRS,
        'followPostRedirects' => CURLOPT_POSTREDIR,
        'allowedProtocols' => CURLOPT_PROTOCOLS,
        'allowedRedirectionProtocols' => CURLOPT_PROTOCOLS,
        'httpProxyTunnel' => CURLOPT_HTTPPROXYTUNNEL,
        'proxyAuthentication' => CURLOPT_PROXYAUTH,
        'proxyPort' => CURLOPT_PROXYPORT,
        'proxyType' => CURLOPT_PROXYTYPE,
        'proxy' => CURLOPT_PROXY,
        'proxyCredentials' => CURLOPT_PROXYUSERPWD,
        'freshCookieSession' => CURLOPT_COOKIESESSION,
        'cookie' => CURLOPT_COOKIE,
        'cookieFile' => CURLOPT_COOKIEFILE,
        'cookieJar' => CURLOPT_COOKIEJAR,
        'freshConnection' => CURLOPT_FRESH_CONNECT,
        'noConnectionReuse' => CURLOPT_FORBID_REUSE,
        'tcpNoDelay' => CURLOPT_TCP_NODELAY,
        'connectionTimeout' => CURLOPT_CONNECTTIMEOUT_MS,
        'interface' => CURLOPT_INTERFACE,
        'port' => CURLOPT_PORT,
        'outputCertificateInfoToStdError' => CURLOPT_CERTINFO,
        'sslVerifyPeer' => CURLOPT_SSL_VERIFYPEER,
        'sslVerifyHost' => CURLOPT_SSL_VERIFYHOST,
        'sslVersion' => CURLOPT_SSLVERSION,
        'caInfo' => CURLOPT_CAINFO,
        'caPath' => CURLOPT_CAPATH,
        'keyPassword' => CURLOPT_KEYPASSWD,
        'randomSeedFile' => CURLOPT_RANDOM_FILE,
        'sslCipherList' => CURLOPT_SSL_CIPHER_LIST,
        'sslCertificate' => CURLOPT_SSLCERT,
        'sslCertificatePassword' => CURLOPT_SSLCERTPASSWD,
        'sslCertificateType' => CURLOPT_SSLCERTTYPE,
        'sslEngine' => CURLOPT_SSLENGINE,
        'sslEngineDefault' => CURLOPT_SSLENGINE_DEFAULT,
        'sslKey' => CURLOPT_SSLKEY,
        'sslKeyPassword' => CURLOPT_SSLKEYPASSWD,
        'sslKeyType' => CURLOPT_SSLKEYTYPE,
        'useDnsGlobalCache' => CURLOPT_DNS_USE_GLOBAL_CACHE,
        'dnsCacheTimeout' => CURLOPT_DNS_CACHE_TIMEOUT,
        'ipResolve' => CURLOPT_IPRESOLVE,
        'errorStream' => CURLOPT_STDERR,
        'failOnErrorStatus' => CURLOPT_DNS_USE_GLOBAL_CACHE,
        'successAliases' => CURLOPT_HTTP200ALIASES,
        'noBody' => CURLOPT_NOBODY,
        'noProgress' => CURLOPT_NOPROGRESS,
        'isVerbose' => CURLOPT_VERBOSE,
        'timeout' => CURLOPT_TIMEOUT_MS,
        'encoding' => CURLOPT_ENCODING,
        'useNetRc' => CURLOPT_NETRC,
        'unrestrictedAuthentication' => CURLOPT_UNRESTRICTED_AUTH,
        'httpAuthentication' => CURLOPT_HTTPAUTH,
        'credentials' => CURLOPT_USERPWD,
        'upload' => CURLOPT_UPLOAD,
        'suggestedBufferSize' => CURLOPT_BUFFERSIZE,
        'expectedInputSize' => CURLOPT_INFILESIZE,
        'maxReceiveSpeed' => CURLOPT_MAX_RECV_SPEED_LARGE,
        'maxSendSpeed' => CURLOPT_MAX_SEND_SPEED_LARGE,
        'outputStream' => CURLOPT_FILE,
        'inputStream' => CURLOPT_INFILE,
        'outputHeaderStream' => CURLOPT_WRITEHEADER,
        'httpVersion' => CURLOPT_HTTP_VERSION,
        'url' => CURLOPT_URL,
        'method' => CURLOPT_CUSTOMREQUEST,
        'timeCondition' => CURLOPT_TIMECONDITION,
        'timeValue' => CURLOPT_TIMEVALUE,
        'resumeFrom' => CURLOPT_RESUME_FROM,
        'range' => CURLOPT_RANGE,
        'referer' => CURLOPT_REFERER,
        'userAgent' => CURLOPT_USERAGENT,
        'headers' => CURLOPT_HTTPHEADER,
        'onHeader' => CURLOPT_HEADERFUNCTION,
        'onProgress' => CURLOPT_PROGRESSFUNCTION,
        'onRead' => CURLOPT_READFUNCTION,
        'onWrite' => CURLOPT_WRITEFUNCTION,
    ];

    #endregion

    #region // Redirection options

    /**
     * CURLOPT_FOLLOWLOCATION
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html
     * @var bool $followRedirects
     */
    public $followRedirects;

    /**
     * CURLOPT_MAXREDIRS
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_MAXREDIRS.html
     * @var int $maxRedirects
     */
    public $maxRedirects;

    /**
     * CURLOPT_POSTREDIR (flags)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_POSTREDIR.html
     * @var int $followPostRedirects
     */
    public $followPostRedirects;

    /**
     * CURLOPT_PROTOCOLS (flags; CURLPROTO_*)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROTOCOLS.html
     * @var int $allowedProtocols
     */
    public $allowedProtocols;

    /**
     * CURLOPT_PROTOCOLS (flags; CURLPROTO_*)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROTOCOLS.html
     * @var int $allowedRedirectionProtocols
     */
    public $allowedRedirectionProtocols;

    #endregion

    #region // Proxy options

    /**
     * CURLOPT_HTTPPROXYTUNNEL
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_HTTPPROXYTUNNEL.html
     * @var bool $httpProxyTunnel
     */
    public $httpProxyTunnel;

    /**
     * CURLOPT_PROXYAUTH (flags; CURLAUTH_BASIC|CURLAUTH_NTLM)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROXYAUTH.html
     * @var int $proxyAuthentication
     */
    public $proxyAuthentication;

    /**
     * CURLOPT_PROXYPORT
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROXYPORT.html
     * @var int $proxyPort
     */
    public $proxyPort;

    /**
     * CURLOPT_PROXYTYPE (flags; CURLPROXY_*)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROXYTYPE.html
     * @var int $proxyType
     */
    public $proxyType;

    /**
     * CURLOPT_PROXY
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html
     * @var string $proxy
     */
    public $proxy;

    /**
     * CURLOPT_PROXYUSERPWD
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROXYUSERPWD.html
     * @var string $proxyCredentials
     */
    public $proxyCredentials;

    #endregion

    #region // Cookie options

    /**
     * CURLOPT_COOKIESESSION
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_COOKIESESSION.html
     * @var bool $freshCookieSession
     */
    public $freshCookieSession;

    /**
     * CURLOPT_COOKIE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_COOKIE.html
     * @var string $cookie
     */
    public $cookie;

    /**
     * CURLOPT_COOKIEFILE (file)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_COOKIEFILE.html
     * @var string $cookieFile
     */
    public $cookieFile;

    /**
     * CURLOPT_COOKIEJAR (file)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_COOKIEJAR.html
     * @var string $cookieJar
     */
    public $cookieJar;

    #endregion

    #region // Connection options

    /**
     * CURLOPT_FRESH_CONNECT
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_FRESH_CONNECT.html
     * @var bool $freshConnection
     */
    public $freshConnection;

    /**
     * CURLOPT_FORBID_REUSE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_FORBID_REUSE.html
     * @var bool $noConnectionReuse
     */
    public $noConnectionReuse;

    /**
     * CURLOPT_TCP_NODELAY
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_TCP_NODELAY.html
     * @var bool $tcpNoDelay
     */
    public $tcpNoDelay;

    /**
     * CURLOPT_CONNECTTIMEOUT_MS (milliseconds)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_CONNECTTIMEOUT_MS.html
     * @var int $connectionTimeout
     */
    public $connectionTimeout;

    /**
     * CURLOPT_INTERFACE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_INTERFACE.html
     * @var string $interface
     */
    public $interface;

    /**
     * CURLOPT_PORT
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PORT.html
     * @var int $port
     */
    public $port;

    #endregion

    #region // SSL/TLS options

    /**
     * CURLOPT_CERTINFO
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_CERTINFO.html
     * @var bool $outputCertificateInfoToStdError
     */
    public $outputCertificateInfoToStdError;

    /**
     * CURLOPT_SSL_VERIFYPEER
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html
     * @var bool $sslVerifyPeer
     */
    public $sslVerifyPeer;

    /**
     * CURLOPT_SSL_VERIFYHOST
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html
     * @var int $sslVerifyHost
     */
    public $sslVerifyHost;

    /**
     * CURLOPT_SSLVERSION
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLVERSION.html
     * @var int $sslVersion
     */
    public $sslVersion;

    /**
     * CURLOPT_CAINFO (file)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_CAINFO.html
     * @var string $caInfo
     */
    public $caInfo;

    /**
     * CURLOPT_CAPATH (directory)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html
     * @var string $caPath
     */
    public $caPath;

    /**
     * CURLOPT_KEYPASSWD
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html
     * @var string $keyPassword
     */
    public $keyPassword;

    /**
     * CURLOPT_RANDOM_FILE (file)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_RANDOM_FILE.html
     * @var string $randomSeedFile
     */
    public $randomSeedFile;

    /**
     * CURLOPT_SSL_CIPHER_LIST
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSL_CIPHER_LIST.html
     * @var string $sslCipherList
     */
    public $sslCipherList;

    /**
     * CURLOPT_SSLCERT (file)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html
     * @var string $sslCertificate
     */
    public $sslCertificate;

    /**
     * CURLOPT_SSLCERTPASSWD
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERTPASSWD.html
     * @var string $sslCertificatePassword
     */
    public $sslCertificatePassword;

    /**
     * CURLOPT_SSLCERTTYPE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERTTYPE.html
     * @var string $sslCertificateType
     */
    public $sslCertificateType;

    /**
     * CURLOPT_SSLENGINE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLENGINE.html
     * @var string $sslEngine
     */
    public $sslEngine;

    /**
     * CURLOPT_SSLENGINE_DEFAULT
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLENGINE_DEFAULT.html
     * @var string $sslEngineDefault
     */
    public $sslEngineDefault;

    /**
     * CURLOPT_SSLKEY (file)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html
     * @var string $sslKey
     */
    public $sslKey;

    /**
     * CURLOPT_SSLKEYPASSWD
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEYPASSWD.html
     * @var string $sslKeyPassword
     */
    public $sslKeyPassword;

    /**
     * CURLOPT_SSLKEYTYPE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEYTYPE.html
     * @var string $sslKeyType
     */
    public $sslKeyType;

    #endregion

    #region // Internal behaviour options

    /**
     * CURLOPT_DNS_USE_GLOBAL_CACHE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_DNS_USE_GLOBAL_CACHE.html
     * @var bool $useDnsGlobalCache
     */
    public $useDnsGlobalCache = false;

    /**
     * CURLOPT_DNS_CACHE_TIMEOUT (seconds)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_DNS_CACHE_TIMEOUT.html
     * @var int $dnsCacheTimeout
     */
    public $dnsCacheTimeout;

    /**
     * CURLOPT_IPRESOLVE (flags; CURL_IPRESOLVE_*)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_IPRESOLVE.html
     * @var int $ipResolve
     */
    public $ipResolve;

    /**
     * CURLOPT_STDERR (resource)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_STDERR.html
     * @var resource $errorStream
     */
    public $errorStream;

    #endregion

    #region // External behaviour options

    /**
     * CURLOPT_DNS_USE_GLOBAL_CACHE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_DNS_USE_GLOBAL_CACHE.html
     * @var bool $failOnError
     */
    public $failOnErrorStatus;

    /**
     * CURLOPT_HTTP200ALIASES
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_HTTP200ALIASES.html
     * @var array $successAliases
     */
    public $successAliases;

    /**
     * CURLOPT_NOBODY
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_NOBODY.html
     * @var bool $noBody
     */
    public $noBody;

    /**
     * CURLOPT_NOPROGRESS
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_NOPROGRESS.html
     * @var bool $noProgress
     */
    public $noProgress = true;

    /**
     * CURLOPT_VERBOSE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_VERBOSE.html
     * @var bool $isVerbose
     */
    public $isVerbose;

    /**
     * CURLOPT_TIMEOUT_MS (milliseconds)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_TIMEOUT_MS.html
     * @var int $timeout
     */
    public $timeout;

    /**
     * CURLOPT_ENCODING
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_ENCODING.html
     * @var string $encoding
     */
    public $encoding;

    #endregion

    #region // Authentication region

    /**
     * CURLOPT_NETRC
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_NETRC.html
     * @var bool $useNetRc
     */
    public $useNetRc;

    /**
     * CURLOPT_UNRESTRICTED_AUTH
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_UNRESTRICTED_AUTH.html
     * @var bool $crossDomainAuthentication
     */
    public $unrestrictedAuthentication;

    /**
     * CURLOPT_HTTPAUTH (flag; CURLAUTH_*)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_HTTPAUTH.html
     * @var int $httpAuthentication
     */
    public $httpAuthentication;

    /**
     * CURLOPT_USERPWD
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_USERPWD.html
     * @var string $credentials
     */
    public $credentials;

    #endregion

    #region // Transfer options

    /**
     * CURLOPT_UPLOAD
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_UPLOAD.html
     * @var bool $upload
     */
    public $upload;

    /**
     * CURLOPT_BUFFERSIZE (bytes)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_BUFFERSIZE.html
     * @var int $suggestedBufferSize
     */
    public $suggestedBufferSize;

    /**
     * CURLOPT_INFILESIZE (bytes)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_INFILESIZE.html
     * @var int $expectedInputSize
     */
    public $expectedInputSize;

    /**
     * CURLOPT_MAX_RECV_SPEED_LARGE (bytes per second)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_MAX_RECV_SPEED_LARGE.html
     * @var int $maxReceiveSpeed
     */
    public $maxReceiveSpeed;

    /**
     * CURLOPT_MAX_SEND_SPEED_LARGE (bytes per second)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_MAX_SEND_SPEED_LARGE.html
     * @var int $maxReceiveSpeed
     */
    public $maxSendSpeed;

    /**
     * CURLOPT_FILE (resource)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_FILE.html
     * @var resource $outputStream
     */
    public $outputStream;

    /**
     * CURLOPT_INFILE (resource)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_INFILE.html
     * @var resource $inputStream
     */
    public $inputStream;

    /**
     * CURLOPT_WRITEHEADER (resource)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_WRITEHEADER.html
     * @var resource $outputHeaderStream
     */
    public $outputHeaderStream;

    #endregion

    #region // Request options

    /**
     * CURLOPT_HTTP_VERSION (flags; CURL_HTTP_VERSION_*)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_HTTP_VERSION.html
     * @var int $httpVersion
     */
    public $httpVersion;

    /**
     * CURLOPT_URL
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_URL.html
     * @var string $uri
     */
    public $url;

    /**
     * CURLOPT_CUSTOMREQUEST
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_CUSTOMREQUEST.html
     * @var string $httpMethod
     */
    public $method;

    /**
     * CURLOPT_TIMECONDITION (flags; CURL_TIMECOND_)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_TIMECONDITION.html
     * @var int $timeCondition
     */
    public $timeCondition;

    /**
     * CURLOPT_TIMEVALUE (timestamp)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_TIMEVALUE.html
     * @var int $timeValue
     */
    public $timeValue;

    /**
     * CURLOPT_RESUME_FROM (bytes)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_RESUME_FROM.html
     * @var int $resumeFrom
     */
    public $resumeFrom;

    /**
     * CURLOPT_RANGE
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_RANGE.html
     * @var string $range
     */
    public $range;

    /**
     * CURLOPT_REFERER
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_REFERER.html
     * @var string $referer
     */
    public $referer;

    /**
     * CURLOPT_USERAGENT
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_USERAGENT.html
     * @var string $userAgent
     */
    public $userAgent;

    /**
     * CURLOPT_HTTPHEADER
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html
     * @var array $headers
     */
    public $headers;

    #endregion

    #region // Callbacks

    /**
     * CURLOPT_HEADERFUNCTION (callable)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_HEADERFUNCTION.html
     * @var Callable $onHeader
     */
    public $onHeader;

    /**
     * CURLOPT_PROGRESSFUNCTION (callable)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_PROGRESSFUNCTION.html
     * @var Callable $onProgress
     */
    public $onProgress;

    /**
     * CURLOPT_READFUNCTION (callable)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_READFUNCTION.html
     * @var Callable $onRead
     */
    public $onRead;

    /**
     * CURLOPT_WRITEFUNCTION (callable)
     * @link http://curl.haxx.se/libcurl/c/CURLOPT_WRITEFUNCTION.html
     * @var Callable $onWrite
     */
    public $onWrite;

    #endregion

    /**
     * Creates an instance of CurlyOptions, optionally providing an array of values to pre-configure.
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $class = new ReflectionClass(__CLASS__);

        foreach ($options as $option => $value) {
            $property = $class->getProperty($option);
            if ($property !== null) {
                $property->setValue($this, $value);
            }
        }
    }

    /**
     * Converts the options configured in this object to an array that is compatible with curl_setopt_array().
     *
     * @param bool $useNativeCurlConstants
     *
     * @return array
     */
    public function toArray($useNativeCurlConstants = true): array
    {
        $class = new ReflectionClass(__CLASS__);

        $array = [];
        foreach ($class->getProperties() as $property) {
            if (!array_key_exists($property->name, self::OPTIONS_MAP)) {
                continue;
            }

            $value = $property->getValue($this);
            if ($value !== null) {
                if ($useNativeCurlConstants) {
                    $array[self::OPTIONS_MAP[$property->name]] = $value;
                } else {
                    $array[$property->name] = $value;
                }
            }
        }
        return $array;
    }

    /**
     * Applies the options configured in this object on the provided channel.
     *
     * @param resource $channel
     */
    public function apply($channel): void
    {
        curl_setopt_array($channel, $this->toArray());
    }
}
